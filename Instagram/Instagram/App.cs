﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using DLToolkit.Forms.Controls;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;

namespace Instagram
{
    #region Application

    public class App : Application
    {
        public App()
        {
            // The root page of your application
            MainPage = new NavigationPage(new MainPage());
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }

    #endregion

    #region Pages

    public class MainPage : ContentPage
    {
        private string _clientId = "8f285a0c000c41f68d1b2d6ba812fb28";
        private string _redirectUri = "http://ustimov.org";
        private string _authUrl;

        public MainPage()
        {
            Title = "Instagram";

            _authUrl = $"https://api.instagram.com/oauth/authorize/?client_id={_clientId}&redirect_uri={_redirectUri}&response_type=token";
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (Content is FlowListView)
            {
                return;
            }

            var webView = new WebView
            {
                Source = _authUrl,
                VerticalOptions = LayoutOptions.FillAndExpand,
            };

            webView.Navigating += ProcessLogin;

            Content = webView;
        }

        private async void ProcessLogin(object sender, WebNavigatingEventArgs args)
        {
            if (!args.Url.StartsWith(_redirectUri))
            {
                return;
            }

            args.Cancel = true;

            var token = new Uri(args.Url).Fragment.Split('=')[1];

            var client = new HttpClient();

            var response =
                await client.GetStringAsync($"https://api.instagram.com/v1/users/self/media/recent?access_token={token}");

            var jsonObj = JObject.Parse(response);

            var mediaObjects = (from media in jsonObj["data"]
                                select new Media
                                {
                                    Id = media["id"].ToString(),
                                    ThumbnailUrl = media["images"]["thumbnail"]["url"].ToString(),
                                    StandartUrl = media["images"]["standard_resolution"]["url"].ToString(),
                                }).ToList();

            var listView = new FlowListView
            {
                FlowColumnsTemplates = new List<FlowColumnTemplateSelector>
                {
                    new FlowColumnSimpleTemplateSelector { ViewType = typeof(ImageView) },
                    new FlowColumnSimpleTemplateSelector { ViewType = typeof(ImageView) },
                    new FlowColumnSimpleTemplateSelector { ViewType = typeof(ImageView) },
                },
                FlowItemsSource = mediaObjects,
                RowHeight = 117,
            };

            listView.FlowItemTapped += async (o, eventArgs) =>
                await Navigation.PushAsync(new DetailPage(token, eventArgs.Item as Media));

            Content = listView;

            ToolbarItems.Add(new ToolbarItem("Выход", null, () =>
            {
                ToolbarItems.Clear();

                var webView = new WebView
                {
                    Source = "https://instagram.com/accounts/logout/",
                    VerticalOptions = LayoutOptions.FillAndExpand,
                };

                Content = webView;

                webView.Navigated += RedirectToLogin;
            }));
        }

        private void RedirectToLogin(object sender, WebNavigatedEventArgs e)
        {
            var webView = sender as WebView;
            if (webView == null)
            {
                return;
            }
            webView.Navigating += ProcessLogin;
            webView.Navigated -= RedirectToLogin;
            webView.Source = _authUrl;
        }
    }

    public class DetailPage : ContentPage
    {
        private string _token;
        private Media _media;
        private ListView _listView;

        public DetailPage(string token, Media media)
        {
            Title = "Фото";
            Padding = new Thickness(10);

            _token = token;
            _media = media;

            _listView = new ListView
            {
                ItemTemplate = new DataTemplate(() =>
                {
                    var textCell = new TextCell();
                    textCell.SetBinding<Comment>(TextCell.TextProperty, v => v.Author);
                    textCell.SetBinding<Comment>(TextCell.DetailProperty, v => v.Text);
                    return textCell;
                }),
            };

            var entry = new Entry { BackgroundColor = Color.White };

            var button = new Button { Text = "Отправить" };

            button.Clicked += async (sender, args) =>
            {
                if (string.IsNullOrEmpty(entry.Text))
                {
                    await DisplayAlert("Ошибка", "Нельзя отправлять пустое сообщение", "Ok");
                    return;
                }

                var client = new HttpClient();

                var content = new FormUrlEncodedContent(new Dictionary<string, string> { { "text", entry.Text } });

                var response = await client.PostAsync($"https://api.instagram.com/v1/media/{_media.Id}/comments?access_token={_token}",
                    content);

                entry.Text = string.Empty;

                await LoadComments();
            };

            Content = new StackLayout
            {
                Children =
                {
                    new Image
                    {
                        Source = media.StandartUrl,
                        HeightRequest = 250,
                    },
                    _listView,
                    new Label { Text = "Введите комментарий:" },
                    entry,
                    button,
                }
            };
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            await LoadComments();
        }

        private async Task LoadComments()
        {
            var client = new HttpClient();

            var response = await client.GetStringAsync($"https://api.instagram.com/v1/media/{_media.Id}/comments?access_token={_token}");

            var jsonObj = JObject.Parse(response);

            var comments = (from comment in jsonObj["data"]
                            select new Comment
                            {
                                Author = comment["from"]["username"].ToString(),
                                Text = comment["text"].ToString(),
                            }).ToList();

            _listView.ItemsSource = comments;
            if (comments.Count > 1)
            {
                _listView.ScrollTo(comments[comments.Count - 1], ScrollToPosition.End, false);
            }
        }
    }

    #endregion

    #region Views

    public class ImageView : ContentView
    {
        public ImageView()
        {
            var image = new Image();
            image.SetBinding<Media>(Image.SourceProperty, v => v.ThumbnailUrl);
            Content = image;
        }
    }

    #endregion

    #region Models

    public class Comment
    {
        public string Author { get; set; }

        public string Text { get; set; }
    }

    public class Media
    {
        public string Id { get; set; }

        public string ThumbnailUrl { get; set; }

        public string StandartUrl { get; set; }
    }

    #endregion
}
